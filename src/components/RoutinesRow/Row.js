import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Easing,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import { Left } from 'native-base';
import { LineChart, Grid } from 'react-native-svg-charts';
// import Parallax from "react-native-parallax";

const window = Dimensions.get('window');
const styles = StyleSheet.create({
  mainCategoryText: {
    color: 'white',
    fontSize: 25,
    // fontFamily: 'Baloo',
  },
  image: {
    flex: 1,
    marginLeft: -30,
    marginRight: -30,
    // width: window.width,
    height: window.width / 2 - 40,
    marginTop: 8,
    borderRadius: 5,
    // backgroundColor: 'transparent',
    marginBottom: 12,
  },
  overlay: {
    // alignItems: "center",
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  containerStyle: {
    shadowColor: '#000',
    backgroundColor: 'transparent',
    shadowOpacity: 0.4,
    shadowRadius: 8,
    shadowOffset: { width: 0, height: 12 },
    elevation: 10,
  },
  footer: {
    position: 'absolute',
    padding: 2,
    left: 0,
    right: 0,
    bottom: 0,
    height: 30,
    display: 'flex',
    backgroundColor: '#000',
    opacity: 0.5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 16,
    height: 80,
    flex: 1,
    marginTop: 7,
    marginBottom: 12,
    borderRadius: 4,
    width: window.width - 30 * 2,
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOpacity: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowRadius: 2,
  },
  text: {
    fontSize: 24,
    color: '#222222',
  },
});
class Row extends Component {
  constructor(props) {
    super(props);

    this._active = new Animated.Value(0);

    this._style = {
      transform: [
        {
          scale: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 1.1],
          }),
        },
      ],
      shadowRadius: this._active.interpolate({
        inputRange: [0, 1],
        outputRange: [2, 10],
      }),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }

  render() {
    const { data, index, onExerciseClick, weightCountObj = {} } = this.props;
    const textStyle = { marginLeft: 30, textAlign: 'left', fontWeight: 'bold' };
    const activeTextStyle = {
      marginRight: 30,
      textAlign: 'right',
      fontWeight: 'bold',
      fontSize: 30,
    };

    const categoryImage = data.imageSource;
    // const mockData = [150];
    return (
      <TouchableOpacity onPress={() => onExerciseClick(data)}>
        <Animated.View>
          <ImageBackground
            key={index}
            style={styles.image}
            // overlayStyle={styles.overlay}
            containerStyle={styles.containerStyle}
            blurRadius={data.activeRoutine ? 10 : 0}
            // parallaxFactor={0.4}
            source={categoryImage}
          >
            <View
              style={{
                display: 'flex',
                backgroundColor: '#000',
                opacity: 0.6,
                justifyContent: 'center',
                height: window.width / 2 - 40,
              }}
            >
              <View style={{ marginTop: 20, flexDirection: 'row' }}>
                <Left>
                  <Text style={[styles.mainCategoryText, { ...textStyle }]}>{data.name}</Text>
                </Left>
                {data.activeRoutine && (
                  <Text style={[styles.mainCategoryText, { ...activeTextStyle }]}>Active</Text>
                )}
              </View>
              <LineChart
                style={{
                  height: 120,
                  bottom: 15,
                }}
                animate
                data={weightCountObj && weightCountObj[data.id] ? weightCountObj[data.id] : []}
                svg={{ stroke: 'rgb(134, 65, 244)', strokeWidth: 4 }}
                contentInset={{ top: 20, bottom: 20 }}
              >
                <Grid />
              </LineChart>
            </View>
          </ImageBackground>
        </Animated.View>
      </TouchableOpacity>
    );
  }
}
Row.propTypes = {
  active: PropTypes.bool,
  data: PropTypes.object,
  index: PropTypes.number,
  onExerciseClick: PropTypes.func,
  weightCountObj: PropTypes.object,
};
export default Row;
