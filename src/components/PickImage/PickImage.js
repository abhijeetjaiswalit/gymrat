import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, Image, StyleSheet, TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { Button, Text } from 'native-base';

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    width: '100%',
    alignItems: 'center',
  },
  placeholder: {
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: '#eee',
    width: '80%',
    height: 150,
  },
  button: {
    margin: 8,
    width: '80%',
  },
  previewImage: {
    width: '100%',
    height: '100%',
  },
  previewText: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
});
class PickImage extends Component {
  state = {
    pickedImaged: null,
    routine: '',
  };

  pickImageHandler = () => {
    ImagePicker.showImagePicker({ title: 'Pick an Image' }, res => {
      if (res.didCancel) {
        // eslint-disable-next-line no-console
        console.log('User cancelled!');
      } else if (res.error) {
        // eslint-disable-next-line no-console
        console.log('Error', res.error);
      } else {
        // const source = { uri: `data:image/jpeg;base64,${res.data}` };
        this.setState({
          pickedImaged: { uri: res.uri },
        });
        // this.props.onImagePicked({uri: res.uri, base64: res.data});
      }
    });
  };

  handeRoutineAdd = () => {
    const { routine, pickedImaged } = this.state;
    this.props.onRoutineAdd(pickedImaged, routine).then(() => {
      this.setState({
        pickedImaged: null,
        routine: '',
      });
    });
  };

  render() {
    const { pickedImaged } = this.state;
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.placeholder} onPress={this.pickImageHandler}>
          <View style={styles.previewText}>
            {pickedImaged && <Image source={this.state.pickedImaged} style={styles.previewImage} />}
            {!pickedImaged && <Text>Press here to pick an image.</Text>}
          </View>
        </TouchableOpacity>
        <View style={styles.button}>
          <TextInput
            placeholder="New Routine"
            style={{
              height: 40,
              borderColor: 'black',
              backgroundColor: '#eee',
              // borderColor: 'gray',
              borderWidth: 1,
            }}
            onChangeText={routine => this.setState({ routine })}
            value={this.state.routine}
          />
        </View>
        <View style={styles.button}>
          <Button bordered full onPress={this.handeRoutineAdd}>
            <Text>Submit</Text>
          </Button>
        </View>
      </View>
    );
  }
}
PickImage.propTypes = {
  onRoutineAdd: PropTypes.func,
};
export default PickImage;
