import React from 'react';
import PropTypes from 'prop-types';
import { Animated, Easing, StyleSheet, Dimensions, Platform } from 'react-native';
import { Input, Icon } from 'native-base';

const window = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',

    ...Platform.select({
      ios: {
        paddingTop: 20,
      },
    }),
  },

  title: {
    fontSize: 20,
    paddingVertical: 20,
    color: '#999999',
  },

  list: {
    flex: 1,
  },

  contentContainer: {
    width: window.width,

    ...Platform.select({
      ios: {
        paddingHorizontal: 30,
      },

      android: {
        paddingHorizontal: 0,
      },
    }),
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    // padding: 16,
    height: 40,
    flex: 1,
    marginTop: 7,
    marginBottom: 12,
    borderRadius: 4,
    width: window.width - 30 * 2,
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOpacity: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowRadius: 2,
  },
  activeRow: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'yellow',
    // padding: 16,
    height: 40,
    flex: 1,
    marginTop: 7,
    marginBottom: 12,
    borderRadius: 4,
    width: window.width - 30 * 2,
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOpacity: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowRadius: 2,
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 30,
    borderRadius: 25,
  },

  text: {
    fontSize: 22,
    color: '#222222',
  },
  activeText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#222222',
  },
});
class RowComponent extends React.Component {
  constructor(props) {
    super(props);

    this._active = new Animated.Value(0);

    this._style = {
      transform: [
        {
          scale: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 1.1],
          }),
        },
      ],
      shadowRadius: this._active.interpolate({
        inputRange: [0, 1],
        outputRange: [2, 10],
      }),
      flex: 1,
      justifyContent: 'center',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }

  render() {
    const { data, lastIndex, index } = this.props;
    return (
      <Animated.View style={[index === lastIndex ? styles.activeRow : styles.row, this._style]}>
        {/* <Image source={{ uri: data.image }} style={styles.image} /> */}
        {/* <Text style={styles.text}>{data.text}</Text> */}
        <Icon type="FontAwesome5" name="list" style={{ fontSize: 23, marginLeft: 5 }} />
        <Input
          placeholder="Exercise"
          defaultValue={data.name}
          style={index === lastIndex ? styles.activeText : styles.text}
          textAlign="center"
          disabled
        />
      </Animated.View>
    );
  }
}
RowComponent.propTypes = {
  active: PropTypes.bool,
  index: PropTypes.number,
  lastIndex: PropTypes.number,
  data: PropTypes.object,
};

export default RowComponent;
