import React from 'react';
import { StatusBar, Dimensions, Platform } from 'react-native';
import { Root } from 'native-base';

import { AppNavigator } from './navigator';

const { width, height } = Dimensions.get('window');

const isIphoneX =
  Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS && (height === 812 || width === 812);

export default class App extends React.PureComponent {
  state = {};

  render() {
    return (
      <Root>
        <StatusBar hidden={!isIphoneX} />
        <AppNavigator />
      </Root>
    );
  }
}
