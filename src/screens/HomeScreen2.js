/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import { StyleSheet } from 'react-native';
import { Container, Header, Tab, Tabs, TabHeading, Text } from 'native-base';
import Tab1 from './RoutineScreen';

type Props = {};
export default class HomeScreen2 extends Component<Props> {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header hasTabs />
        <Tabs>
          <Tab
            heading={
              <TabHeading>
                <Text>My Routines</Text>
              </TabHeading>
            }
          >
            <Tab1 navigation={navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
HomeScreen2.propTypes = {
  navigation: PropTypes.object,
};
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },

//   backgroundImage: {
//     flex: 1,
//     resizeMode: 'contain', // or 'stretch'
//   },
// });
