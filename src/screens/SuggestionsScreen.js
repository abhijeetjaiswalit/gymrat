import React, { Component } from 'react';
import { Title, Container, Button, Text, Body, Header, Content, Textarea, Form } from 'native-base';
import { Linking } from 'react-native';

export default class SuggestionBox extends Component {
  state = {
    suggestion: null,
  };

  handleSubmitSuggestion = () => {
    const { suggestion } = this.state;
    Linking.openURL(`mailto:gymmouse@zaycare.com?subject=SendMail&body=${suggestion}`);
  };

  handleSuggestionChange = text => {
    this.setState({
      suggestion: text,
    });
  };

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Suggestion Box</Title>
          </Body>
        </Header>
        <Content padder>
          <Form>
            <Textarea
              rowSpan={5}
              bordered
              placeholder="Suggestions"
              onChangeText={text => this.handleSuggestionChange(text)}
            />
            <Button
              block
              primary
              style={{ marginTop: 5 }}
              onPress={this.handleSubmitSuggestion}
              title="gymmouse@zaycare.com"
            >
              <Text>Save</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
