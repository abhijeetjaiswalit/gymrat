/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image } from 'react-native';
import { Container, Tab, Tabs, TabHeading, Text } from 'native-base';
import Tab1 from './RoutineScreen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  backgroundImage: {
    flex: 1,
    resizeMode: 'contain', // or 'stretch'
  },
});

type Props = {};
export default class HomeScreen5 extends Component<Props> {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        {/* <Header hasTabs /> */}
        <Tabs>
          <Tab
            heading={
              <TabHeading>
                <Text>My Routines</Text>
              </TabHeading>
            }
          >
            <Tab1 navigation={navigation} />
          </Tab>
          <Tab
            heading={
              <TabHeading>
                <Text>Champions</Text>
              </TabHeading>
            }
          >
            <View style={styles.container}>
              <Image source={require('../images/champions.png')} style={styles.backgroundImage} />
            </View>
          </Tab>
          <Tab
            heading={
              <TabHeading>
                <Text>Groups</Text>
              </TabHeading>
            }
          >
            <View style={styles.container}>
              <Image source={require('../images/groups.png')} style={styles.backgroundImage} />
            </View>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
HomeScreen5.propTypes = {
  navigation: PropTypes.object,
};
