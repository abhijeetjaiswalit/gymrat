/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, ScrollView, View, Dimensions, Platform } from 'react-native';
import { Header, Title, Body, Left, Right } from 'native-base';
import SortableList from 'react-native-sortable-list';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';
import SwitchSelector from 'react-native-switch-selector';
import Row from '../components/RoutinesRow/Row';
import { ROUTINES, ROUTINESIDS } from '../utils/constant';
// import PickImage from '../components/PickImage/PickImage';

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    paddingTop: 20,
  },
  list: {
    flex: 1,
  },
  fill: {
    flex: 1,
    backgroundColor: 'black',
  },
  contentContainer: {
    width: window.width,

    ...Platform.select({
      ios: {
        paddingHorizontal: 30,
      },

      android: {
        paddingHorizontal: 0,
      },
    }),
  },
});

type Props = {};
export default class RoutineScreen extends Component<Props> {
  state = {
    routines: [],
    weightCountObj: null,
  };

  async componentDidMount() {
    // AsyncStorage.clear();
    // const testData = {
    //   "06/21/2019": { "totalWeightForToday": 95 },
    //   "06/22/2019": { "totalWeightForToday": 120 },
    //   "06/23/2019": { "totalWeightForToday": 130 } }
    // await AsyncStorage.setItem('back', JSON.stringify(testData))
    try {
      let hoursDiff = 0;
      const lastVisitedTime = await AsyncStorage.getItem('last-visited-time');
      const exercisePart = await AsyncStorage.getItem('exercisePart');
      this.setState({
        exercisePart,
      });
      if (lastVisitedTime) {
        const currentTime = new Date().getTime();
        hoursDiff = Math.abs(currentTime - parseInt(lastVisitedTime)) / 36e5;
      }
      this.fetchRoutines(
        hoursDiff > 3 ? { isThresholdReached: true } : { isThresholdReached: false }
      );
    } catch (e) {
      // saving error
      // eslint-disable-next-line
      Alert.alert(e.message);
    }
  }

  fetchChartData = async () => {
    const idArr = ROUTINESIDS;
    let weightCountObj = null;
    idArr.map(async id => {
      const weightCountsJson = await AsyncStorage.getItem(id);
      const weightsArr = [];
      if (weightCountsJson) {
        const weightCountsWithDates = JSON.parse(weightCountsJson);
        Object.keys(weightCountsWithDates).map(date => {
          weightsArr.push(weightCountsWithDates[date].totalWeightForToday);
        });
        weightCountObj = {
          ...weightCountObj,
          [id]: weightsArr,
        };
        this.setState({
          weightCountObj,
        });
      }
    });
  };

  fetchRoutines = async ({ isThresholdReached }) => {
    const { routines } = this.state;
    await this.fetchChartData();
    const routinesJson = await AsyncStorage.getItem('@routines');
    if (routinesJson) {
      const routinesArr = JSON.parse(routinesJson);
      if (isThresholdReached) {
        const modArr = routinesArr.map(routine => {
          return {
            ...routine,
            activeRoutine: false,
          };
        });
        this.setState({
          routines: [...modArr],
        });
      } else {
        this.setState({
          routines: [...routinesArr],
        });
      }
    } else {
      await AsyncStorage.setItem('@routines', JSON.stringify(ROUTINES));
      this.setState({
        routines: [...routines, ...ROUTINES],
      });
    }
  };

  _handleExerciseRedirect = async data => {
    const { navigation } = this.props;
    navigation.navigate('Home', {
      exerciseData: data.exercises,
      title: data.name,
      id: data.id,
    });
  };

  _renderRow = ({ data, active, index }) => {
    const { weightCountObj } = this.state;
    return (
      <Row
        data={data}
        active={active}
        index={index}
        onExerciseClick={this._handleExerciseRedirect}
        weightCountObj={weightCountObj}
      />
    );
  };

  _handleRoutineAdd = async (pickedImaged, routine) => {
    const { routines } = this.state;
    const newRoutine = {
      id: routine,
      name: routine.toUpperCase(),
      imageSource: pickedImaged,
      exercises: [],
    };
    this.setState({
      routines: [...routines, newRoutine],
    });
    // eslint-disable-next-line no-return-await
    return await AsyncStorage.setItem('@routines', JSON.stringify(routines));
  };

  handleToggleSelect = async value => {
    this.setState({ exercisePart: value });
    await AsyncStorage.setItem('exercisePart', value.toString());
  };

  render() {
    const { routines, exercisePart } = this.state;
    return (
      <React.Fragment>
        <Header>
          <Left />
          <Body>
            <Title>Routines</Title>
          </Body>
          <Right>
            <SwitchSelector
              initial={0}
              value={exercisePart ? (exercisePart === '0' ? 0 : 1) : 0}
              onPress={value => this.handleToggleSelect(value)}
              hasPadding
              options={[{ label: 'Left', value: 0 }, { label: 'Right', value: 1 }]}
            />
          </Right>
        </Header>
        <ScrollView style={styles.fill}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'black',
              width: '100%',
              alignItems: 'center',
            }}
          >
            {/* <PickImage onRoutineAdd={this._handleRoutineAdd} /> */}
            {routines && routines.length > 0 && (
              <SortableList
                style={styles.list}
                contentContainerStyle={styles.contentContainer}
                data={routines}
                renderRow={this._renderRow}
              />
            )}
          </View>
          <NavigationEvents onDidFocus={this.fetchRoutines} />
        </ScrollView>
      </React.Fragment>
    );
  }
}
RoutineScreen.propTypes = {
  navigation: PropTypes.object,
};
