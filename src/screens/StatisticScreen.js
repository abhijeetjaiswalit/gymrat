/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Header, Body, Title } from 'native-base';
import { PieChart } from 'react-native-svg-charts';
import { G, Line, Circle } from 'react-native-svg';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';

import Row from '../components/RoutinesRow/Row';
import { ROUTINES } from '../utils/constant';

// const window = Dimensions.get('window');
const Labels = ({ slices }) => {
  return slices.map((slice, index) => {
    const { labelCentroid, pieCentroid, data } = slice;
    return (
      <G key={index}>
        <Line
          x1={labelCentroid[0]}
          y1={labelCentroid[1]}
          x2={pieCentroid[0]}
          y2={pieCentroid[1]}
          stroke={data.svg.fill}
        />
        <Circle cx={labelCentroid[0]} cy={labelCentroid[1]} r={15} fill={data.svg.fill} />
      </G>
    );
  });
};
const TextLabels = ({ routines }) => {
  return routines.map((routine, index) => {
    return (
      <Text
        key={index}
        style={{
          textAlign: 'center',
          fontWeight: 'bold',
          fontSize: 20,
          color: routine.svg.fill,
        }}
      >
        {routine.name}: {routine.count}
      </Text>
    );
  });
};

type Props = {};
export default class App extends Component<Props> {
  state = {
    routines: [],
  };

  async componentDidMount() {
    this.refreshRoutines();
  }

  refreshRoutines = async () => {
    const routinesJson = await AsyncStorage.getItem('@routines');
    if (routinesJson) {
      const routinesArr = JSON.parse(routinesJson);
      this.setState({
        routines: [...routinesArr],
        isRoutineCount: true,
      });
    } else {
      this.setState({
        routines: [...ROUTINES],
        isRoutineCount: false,
      });
    }
  };

  _renderRow = ({ data, active, index }) => {
    return (
      <Row
        data={data}
        active={active}
        index={index}
        onExerciseClick={this._handleExerciseRedirect}
      />
    );
  };

  // handleExerciseSelect = (values) => {
  //     console.log('values', values)
  // }
  render() {
    const { routines, isRoutineCount } = this.state;
    return (
      <View>
        <Header>
          <Body>
            <Title>Statistics</Title>
          </Body>
        </Header>
        {/* <View>
                    <Button block primary
                        onPress={() => { this.SectionedMultiSelect._toggleSelector() }}
                        style={{ marginTop: 5 }}>
                        <Text>Show Exercise Stats</Text>
                    </Button>
                    <SectionedMultiSelect
                        items={routines}
                        uniqueKey="id"
                        subKey="exercises"
                        hideSelect
                        showCancelButton
                        iconKey="icon"
                        selectText="Choose exercises.."
                        showDropDowns
                        showChips={false}
                        readOnlyHeadings
                        ModalWithTouchable
                        onToggleSelector={this.handleExerciseSelect}
                        onSelectedItemsChange={selectedItems => {
                            this.setState({ selectedItems });
                        }}
                        selectedItems={this.state.selectedItems}
                        ref={el => {
                            this.SectionedMultiSelect = el;
                        }}
                    />
                </View> */}
        {isRoutineCount && (
          <PieChart
            data={routines}
            valueAccessor={({ item }) => item.count}
            spacing={10}
            innerRadius={20}
            outerRadius={55}
            labelRadius={80}
            style={{ height: 200 }}
          >
            <Labels />
          </PieChart>
        )}
        <TextLabels routines={routines} />
        <NavigationEvents onDidFocus={this.refreshRoutines} />
        {/* <ScrollView style={styles.fill}>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#fff',
                            width: '100%',
                            alignItems: 'center',
                        }}
                    >
                        {routines && routines.length > 0 && (
                            <SortableList
                                style={styles.list}
                                contentContainerStyle={styles.contentContainer}
                                data={routines}
                                renderRow={this._renderRow}
                            />
                        )}
                    </View>
                </ScrollView>  */}
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     flexDirection: 'column',
//   },
//   contentContainer: {
//     width: window.width,
//     paddingHorizontal: 30,
//   },
//   list: {
//     flex: 1,
//   },
// });
