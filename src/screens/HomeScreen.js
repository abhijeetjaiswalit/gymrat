/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { Container, Left, Right, Title, Body, Icon, Header, Button, Toast } from 'native-base';
import Slider from '@react-native-community/slider';
import AsyncStorage from '@react-native-community/async-storage';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import SortableList from 'react-native-sortable-list';
import { NavigationEvents } from 'react-navigation';

import { Timer } from '../components/FlipTimer';
import RowComponent from '../components/RowComponent';
import { saveExercise } from '../utils/api';
import { getDateString, saveTotals } from '../utils/helper';
import { ROUTINESWITHEXERCISES } from '../utils/constant';

const { height } = Dimensions.get('window');

const box_count = 4;
const box_height = height / box_count;
const window = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#F5FCFF',
    flexDirection: 'column',
  },
  box: {
    height: box_height,
  },
  DndContainer: {
    // flex: 4,
    // height: box_height,
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  sliderContainer: {
    justifyContent: 'center',
    height: box_height / 2,
  },
  timerContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'column',
    height: box_height / 2.3,
  },
  addButton: {
    alignSelf: 'center',
    top: 5,
    height: 70,
    width: 70,
    // bottom: 8,
    borderWidth: 1,
    borderColor: 'red',
    borderRadius: 35,
    backgroundColor: 'red',
  },
  multiSelect: {
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    borderRadius: 4,
    width: window.width - 30 * 2,
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOpacity: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowRadius: 2,
  },
  inputBox: {
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    borderRadius: 4,
    width: window.width - 30 * 2,
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOpacity: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowRadius: 2,
  },

  title: {
    fontSize: 20,
    paddingVertical: 20,
    color: '#999999',
  },
  list: {
    flex: 1,
  },
  contentContainer: {
    width: window.width,
    paddingHorizontal: 30,
  },

  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  separatorWrap: {
    // paddingVertical: 15,
    // height:box_height/2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    // flex: 1,
    // bottom: 10
    // bottom: 0
    // top: 40
  },
  separator: {
    borderBottomWidth: 2,
    flexGrow: 1,
    borderColor: 'rgba(0,0,0,0.3)',
  },
  activeButtonStyle: {
    alignSelf: 'center',
    paddingLeft: 35,
    paddingRight: 35,
    height: 70,
    justifyContent: 'space-around',
  },
  inactiveButtonStyle: {
    alignSelf: 'center',
    paddingLeft: 25,
    paddingRight: 25,
    height: 50,
    justifyContent: 'space-around',
  },
  activeButtonText: {
    fontSize: 30,
    color: 'white',
  },
  inactiveButtonText: {
    color: 'black',
  },

  timerButton: {
    height: 40,
    backgroundColor: '#333333',
    width: 120,
    alignItems: 'center',
    justifyContent: 'space-around',
    marginLeft: 5,
    // bottom:1
  },
  timerText: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#cccccc',
  },
});
type Props = {};

export default class HomeScreen extends Component<Props> {
  state = {
    weight: 10,
    showTextBox: false,
    showSubmitExerciseButton: false,
    exercise: '',
    showStart: true,
    play: false,
    stop: true,
    time: 0,
    set: 0,
    title: this.props.navigation.getParam('title', ''),
    id: this.props.navigation.getParam('id', ''),
    showRoutines: true,
    exerciseData: [],
    routines: [],
    selectedItems: [],
    stopTime: null,
    startTime: null,
    currentExerciseData: {},
    previouslyActiveRoutineId: null,
  };

  async componentDidMount() {
    this.setState({
      play: false,
    });
    const lastVisitedTime = AsyncStorage.getItem('last-visited-time');
    if (lastVisitedTime) {
      const currentTime = new Date().getTime();
      const hoursDiff = Math.abs(currentTime - lastVisitedTime) / 36e5;
      if (hoursDiff > 3) {
        this.setState({
          // exerciseData: [],
          time: 0,
          set: 0,
          play: false,
          stop: true,
          weight: 10,
          currentExerciseData: {},
          lastUsedWeight: 0,
        });
      }
    }
    // await AsyncStorage.clear('@routines')
    this.getExercises();
  }

  componentWillReceiveProps(next) {
    const { play, set } = this.state;

    const title = next.navigation.getParam('title');
    const id = next.navigation.getParam('id');

    // const prevProps = prev.navigation.getParam('exerciseData')
    if (id !== this.state.id) {
      this.setState(
        {
          exerciseData: [],
          selectedItems: [],
          title,
          id,
        },
        () => {
          this.getExercises();
        }
      );
    }
    if (!play && set === 0) {
      this.setState({
        showRoutines: true,
      });
      // this.handleExerciseSelect();
    }
  }

  // componentDidUpdate(prevProps) {
  //     // const prevId = prevProps.navigation.getParam('id');
  //     // const currentId = this.props.navigation.getParam('id');

  //     // if (prevId !== currentId) {
  //     //     this.setState({
  //     //         previouslyActiveRoutineId: currentId,
  //     //     });
  //     // }
  // }
  validateRoutineSelection = () => {
    const { id } = this.state;
    const { navigation } = this.props;
    if (!id) {
      return Alert.alert('Alert', 'Please select a routine.', [
        { text: 'OK', onPress: () => navigation.navigate('Home2') },
      ]);
    }
  };

  getSelectedExercises = exercises => {
    const ids = [];
    exercises.map(exercise => {
      ids.push(exercise.id);
    });
    return ids;
  };

  getExercises = async () => {
    const { id } = this.state;
    // const { navigation } = this.props;
    try {
      const routinesJson = await AsyncStorage.getItem('@routines');
      if (routinesJson && id) {
        let lastUsedWeight = null;
        const routinesArr = JSON.parse(routinesJson);
        const savedExerciseData = routinesArr.find(x => x.id === id);
        const activeExerciseIndex =
          savedExerciseData.exercises.length > 0 ? savedExerciseData.exercises.length - 1 : null;
        if (activeExerciseIndex && activeExerciseIndex > -1) {
          lastUsedWeight = await AsyncStorage.getItem(
            savedExerciseData.exercises[activeExerciseIndex].id
          );
        }
        this.setState({
          exerciseData: [...savedExerciseData.exercises],
          routines: [...routinesArr],
          selectedItems:
            savedExerciseData.exercises && savedExerciseData.exercises.length > 0
              ? this.getSelectedExercises(savedExerciseData.exercises)
              : [],
          lastUsedWeight: lastUsedWeight ? JSON.parse(lastUsedWeight) : 0,
        });
      }
    } catch (e) {
      // saving error
      Alert.alert(e.message);
    }
  };

  _toggleTimer = type => {
    const { stop, set, play, showStart } = this.state;
    if (type === 'STOP') {
      this.setState({
        time: 0,
        set: set + 1,
        play: false,
        stop: !stop,
        showStart: !showStart,
        stopTime: new Date().getTime(),
      });
      this._saveExercise();
    }
    if (type === 'START') {
      this.setState({
        play: !play,
        showStart: !showStart,
        startTime: new Date().getTime(),
      });
      AsyncStorage.setItem('last-visited-time', new Date().getTime().toString());
    }
  };

  _renderRow = ({ data, active, index }, lastIndex) => {
    return <RowComponent data={data} active={active} lastIndex={lastIndex} index={index} />;
  };

  _handleTextbox = async () => {
    const { exerciseData, exercise, showSubmitExerciseButton, id } = this.state;
    if (showSubmitExerciseButton) {
      this.setState(
        {
          exerciseData: [
            {
              id: exercise.replace(/\s/g, ''),
              name: exercise,
            },
            ...exerciseData,
          ],
          showSubmitExerciseButton: false,
          showTextBox: false,
        },
        async () => {
          try {
            const routinesJson = await AsyncStorage.getItem('@routines');
            if (routinesJson) {
              const routinesArr = JSON.parse(routinesJson) || [];
              const routinesIndex = routinesArr.findIndex(x => x.id === id);
              const updatedRoutines = [
                ...routinesArr.slice(0, routinesIndex),
                {
                  ...routinesArr[routinesIndex],
                  exercises: [
                    {
                      id: exercise.replace(/\s/g, ''),
                      name: exercise,
                    },
                    ...routinesArr[routinesIndex].exercises.slice(),
                  ],
                },
                ...routinesArr.slice(routinesIndex + 1),
              ];
              await AsyncStorage.setItem('@routines', JSON.stringify(updatedRoutines));
            }
          } catch (e) {
            // saving error
            Alert.alert(e.message);
          }
        }
      );
    }
    this.SectionedMultiSelect._toggleSelector();
  };

  _saveRoutinesData = async () => {
    const { routines, id } = this.state;
    const modifiedRoutines = [];
    routines.map(routine => {
      if (routine.id === id) {
        return modifiedRoutines.push({
          ...routine,
          activeRoutine: true,
          count: routine.count ? routine.count + 1 : 1,
          date: new Date().getTime(),
        });
      }
      return modifiedRoutines.push({
        ...routine,
        activeRoutine: false,
      });
    });
    await AsyncStorage.setItem('@routines', JSON.stringify(modifiedRoutines));
    this.setState({
      routines: [...modifiedRoutines],
    });
  };

  toggleRoutines = async () => {
    const { showRoutines, exerciseData, previouslyActiveRoutineId } = this.state;
    if (showRoutines && exerciseData && exerciseData.length === 0) {
      return Alert.alert('Please add one or more exercises before you go to start a routine.');
    }
    const currentId = this.props.navigation.getParam('id');
    if (previouslyActiveRoutineId && previouslyActiveRoutineId !== currentId) {
      Alert.alert('Confirm', 'Are you sure you want to switch routine?', [
        {
          text: 'OK',
          onPress: async () => {
            this.setState({
              showRoutines: !showRoutines,
              stop: true,
              play: false,
              time: 0,
            });
            this._saveRoutinesData();
          },
        },
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ]);
    } else {
      this.setState({
        showRoutines: !showRoutines,
        stop: true,
        play: false,
        time: 0,
      });
      this._saveRoutinesData();
    }
    this.setState({
      previouslyActiveRoutineId: currentId,
    });
  };

  _saveExercise = () => {
    const { exerciseData, startTime, weight, stopTime, id, currentExerciseData } = this.state;
    const activeItem = exerciseData[exerciseData.length - 1];
    this.setState({
      currentExerciseData: {
        id: activeItem.id,
        routineId: id,
        exercise: activeItem,
        weight,
        sets: [
          {
            startTime,
            stopTime,
          },
          ...(currentExerciseData.sets || []),
        ],
      },
    });
  };

  getUniqueArr = (arr, comp) => {
    const unique = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e])
      .map(e => arr[e]);

    return unique;
  };

  _updateRoutines = async newExercises => {
    const { id, routines } = this.state;
    const uniqueArr = this.getUniqueArr(newExercises, 'id');
    const selectedRoutineIndex = routines.findIndex(r => r.id === id);
    const updatedRoutine = { ...routines[selectedRoutineIndex], exercises: uniqueArr };
    const modifiedRoutines = [
      ...routines.slice(0, selectedRoutineIndex),
      updatedRoutine,
      ...routines.slice(selectedRoutineIndex + 1),
    ];
    await AsyncStorage.setItem('@routines', JSON.stringify(modifiedRoutines));
    return modifiedRoutines;
  };

  resetPreviousExercise = async (state, isPrevState) => {
    const { currentExerciseData, exerciseData, stop, startTime, weight, id } = state;
    const { selectedItems } = this.state;
    if (isPrevState) {
      const activeItem = exerciseData[exerciseData.length - 1];
      const prevExerciseData = {
        id: activeItem.id,
        exercise: activeItem,
        weight: activeItem.weight,
        sets: [
          {
            startTime,
            stopTime: new Date().getTime(),
          },
          ...(currentExerciseData.sets || []),
        ],
      };
      // save exercise data
      await saveExercise({ data: prevExerciseData });
      this.setState({
        exerciseData: [],
        time: 0,
        set: 0,
        play: false,
        stop: !stop,
        weight: 10,
        currentExerciseData: {},
      });
    } else {
      const { weight: selectedWeight, id: exerciseId } = currentExerciseData;
      await AsyncStorage.setItem(exerciseId, JSON.stringify(selectedWeight || 10));
      await saveExercise({ data: currentExerciseData });
      const newExercises = [...exerciseData];
      const lastIndex = newExercises.length - 1;
      const selectedItemIndex = selectedItems.indexOf(newExercises[newExercises.length - 1].id);
      const newSelectedItems = [...selectedItems];
      newSelectedItems.splice(selectedItemIndex, 1);
      newExercises.splice(lastIndex, 1);
      const newLastIndex = newExercises.length - 1;
      const lastUsedWeight =
        newLastIndex > -1 ? await AsyncStorage.getItem(newExercises[newLastIndex].id) : 0;
      this.setState({
        exerciseData: newExercises,
        selectedItems: newSelectedItems,
        time: 0,
        set: 0,
        play: false,
        stop: !stop,
        weight: 10,
        currentExerciseData: {},
        lastUsedWeight: lastUsedWeight ? JSON.parse(lastUsedWeight) : 0,
      });
      // if (newExercises && newExercises.length > 0) await this._updateRoutines(newExercises);
    }
    const todayDate = getDateString();
    // save current date's total exercise's weight that user did for a single routine
    await saveTotals(id, todayDate, weight);
    Toast.show({
      text: 'Previous exercise has been saved!',
      buttonText: 'Okay',
      type: 'success',
      duration: 3000,
    });
  };

  handleExerciseSelect = async modelOpen => {
    const { selectedItems, exerciseData } = this.state;
    const newExerciseData = [];
    const allRoutines = ROUTINESWITHEXERCISES;
    if (!modelOpen || (selectedItems.length > 0 && exerciseData.length === 0)) {
      selectedItems.map(id => {
        allRoutines.map(routine => {
          const newExercise = routine.exercises.find(e => e.id === id);
          if (newExercise && newExercise.id) {
            newExerciseData.push(newExercise);
          }
        });
      });
      // const selectedRoutineIndex = routines.findIndex(r => r.id === existingId);
      const newRoutines = [...exerciseData, ...newExerciseData];
      let modifiedRoutines = [];
      if (newRoutines && newRoutines.length > 0) {
        modifiedRoutines = await this._updateRoutines(newRoutines);
      }
      const uniqueArr = this.getUniqueArr(newRoutines, 'id');
      this.setState({
        exerciseData: [...uniqueArr],
        routines: [...modifiedRoutines],
      });
    }
  };

  getExerciseData = () => {
    const { selectedItems, exerciseData } = this.state;
    const newExerciseData = [];
    const allRoutines = ROUTINESWITHEXERCISES;
    if (selectedItems.length > 0 && exerciseData.length === 0) {
      selectedItems.map(id => {
        allRoutines.map(routine => {
          const newExercise = routine.exercises.find(e => e.id === id);
          if (newExercise && newExercise.id) {
            newExerciseData.push(newExercise);
          }
        });
      });
      const uniqueArr = this.getUniqueArr(newExerciseData, 'id');
      if (uniqueArr.length > 0) {
        return uniqueArr;
      }
    }
    return [];
  };

  handleExerciseScroll = ({ h }) => {
    this.scrollView.scrollToEnd(h);
  };

  handleLayoutChange = ev => {
    const {
      nativeEvent: {
        layout: { y },
      },
    } = ev;
    this.scrollView.scrollToEnd(y);
  };

  render() {
    const {
      exerciseData,
      showTextBox,
      showRoutines,
      play,
      time,
      stop,
      set,
      showStart,
      title,
      weight,
      lastUsedWeight,
      selectedItems,
    } = this.state;
    const {
      navigation: { navigate },
    } = this.props;
    const exerciseArr = this.getUniqueArr(exerciseData, 'id');
    const lastIndex = exerciseArr.length > 0 ? exerciseArr.length - 1 : 0;

    return (
      <Container>
        <Header style={{ margin: 10 }}>
          <Left>
            <Button transparent onPress={() => navigate('Home2')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>{title}</Title>
          </Body>
          <Right />
        </Header>
        <View style={styles.container}>
          <View style={[styles.separatorWrap]}>
            <View style={styles.separator} />
            <Button
              style={{
                padding: 40,
                borderColor: 'grey',
                borderWidth: 2,
              }}
              rounded
              bordered
              onPress={() => this.toggleRoutines()}
            >
              <Text style={{ color: 'grey', fontWeight: 'bold' }}>
                {showRoutines ? 'Ready for work out' : 'Show Routines'}
              </Text>
            </Button>
            <View style={styles.separator} />
          </View>
          {showRoutines && (
            <View style={{ margin: 15 }}>
              <Button
                disabled={showTextBox}
                style={styles.addButton}
                active
                onPress={this._handleTextbox}
              >
                <Icon
                  active
                  name="add"
                  style={{ color: 'white', alignSelf: 'center', fontSize: 40, paddingLeft: 8 }}
                />
              </Button>
            </View>
          )}
          <ScrollView
            contentContainerStyle={{ alignItems: 'center' }}
            style={[showRoutines ? { height: box_height * 3.5 } : styles.box, styles.DndContainer]}
            ref={ref => {
              this.scrollView = ref;
            }}
            onContentSizeChange={(width, h) => this.handleExerciseScroll({ width, h })}
            onLayout={ev => this.handleLayoutChange(ev)}
          >
            <View style={styles.multiSelect}>
              <SectionedMultiSelect
                items={ROUTINESWITHEXERCISES}
                uniqueKey="id"
                subKey="exercises"
                hideSelect
                showCancelButton
                iconKey="icon"
                selectText="Choose exercises.."
                showDropDowns
                showChips={false}
                readOnlyHeadings
                ModalWithTouchable
                onToggleSelector={this.handleExerciseSelect}
                onSelectedItemsChange={selectedItems => {
                  this.setState({ selectedItems });
                }}
                selectedItems={this.state.selectedItems}
                ref={el => {
                  this.SectionedMultiSelect = el;
                }}
              />
            </View>
            {exerciseData && exerciseData.length > 0 && (
              <SortableList
                style={styles.list}
                contentContainerStyle={styles.contentContainer}
                data={exerciseArr.length > 0 ? exerciseArr : []}
                scrollEnabled
                renderRow={row => this._renderRow(row, lastIndex)}
              />
            )}
            {exerciseData.length === 0 && <Text>Add Exercises.</Text>}
          </ScrollView>
          {!showRoutines && (
            <View style={[styles.box, styles.sliderContainer]}>
              <Title
                primary
                style={{
                  alignSelf: 'center',
                  paddingLeft: 25,
                  paddingRight: 25,
                  // height: 50,
                }}
              >
                <Text>
                  Selected Weight: {this.state.weight} KG({lastUsedWeight || 0}KG)
                </Text>
              </Title>
              <Slider
                style={{ width: 300, height: 40 }}
                step={5}
                minimumValue={10}
                maximumValue={200}
                value={weight}
                onValueChange={val => this.setState({ weight: val })}
              />
              {/* <Text>Value:{this.state.weight}</Text> */}
            </View>
          )}
          {!showRoutines && (
            <View style={[styles.box, styles.timerContainer]}>
              <View style={{ flexDirection: 'row' }}>
                <Timer time={time} play={play} stop={stop} />
                <View>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      borderColor: 'rgba(0,0,0,0.2)',
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: 85,
                      height: 85,
                      backgroundColor: !showStart ? 'black' : 'green',
                      borderRadius: 50,
                    }}
                    disabled={!selectedItems || (selectedItems && selectedItems.length === 0)}
                    onPress={() => this._toggleTimer(!showStart ? 'STOP' : 'START')}
                  >
                    <Text
                      style={{
                        fontSize: 25,
                        color: 'white',
                      }}
                    >
                      {!showStart ? 'Stop' : 'Start'}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 10,
                }}
              >
                <Left
                  style={{
                    marginLeft: 25,
                    flexDirection: 'row',
                  }}
                >
                  <Text
                    style={{
                      fontSize: 28,
                      fontWeight: 'bold',
                    }}
                  >
                    Set: {set}
                  </Text>
                  {!play && (
                    <Text
                      style={{
                        fontSize: 25,
                        fontWeight: 'bold',
                        backgroundColor: 'yellow',
                        paddingLeft: 5,
                      }}
                    >
                      Rest
                    </Text>
                  )}
                </Left>
                <Right
                  style={{
                    marginRight: 30,
                  }}
                >
                  <Button
                    disabled={
                      !(set > 0) || !selectedItems || (selectedItems && selectedItems.length === 0)
                    }
                    block
                    onPress={() => this.resetPreviousExercise(this.state)}
                  >
                    <Text>Finish Exercise</Text>
                  </Button>
                </Right>
              </View>
            </View>
          )}
        </View>
        <NavigationEvents onDidFocus={this.validateRoutineSelection} />
      </Container>
    );
  }
}
HomeScreen.propTypes = {
  navigation: PropTypes.object,
};
