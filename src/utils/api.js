import AsyncStorage from '@react-native-community/async-storage';

export const saveExercise = async state => {
  try {
    const serialized = JSON.stringify(state.data);
    const key = new Date().getTime().toString();
    if (serialized) return await AsyncStorage.setItem(key, serialized);
  } catch (err) {
    // console.log('errerrerr', err);
    return Promise.reject(err);
  }
};
