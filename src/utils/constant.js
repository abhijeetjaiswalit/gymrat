export const ROUTINES = [
  {
    id: 'abs',
    key: 1,
    name: 'ABS',
    imageSource: require('../images/abs.png'),
    activeRoutine: false,
    exercises: [],
    svg: { fill: '#a1c4d1' },
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
    count: 0,
  },
  {
    id: 'back',
    key: 2,
    name: 'BACK',
    imageSource: require('../images/back.png'),
    activeRoutine: false,
    exercises: [],
    svg: { fill: '#b39bc8' },
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
    count: 0,
  },
  {
    id: 'cardio',
    key: 6,
    name: 'CARDIO',
    imageSource: require('../images/cardio.jpeg'),
    activeRoutine: false,
    exercises: [],
    svg: { fill: '#b39bc9' },
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
    count: 0,
  },
  {
    id: 'chest',
    key: 3,
    name: 'CHEST',
    activeRoutine: false,
    imageSource: require('../images/chest.png'),
    exercises: [],
    svg: { fill: '#f0ebf4' },
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
    count: 0,
  },
  {
    id: 'legs',
    key: 4,
    name: 'LEGS',
    activeRoutine: false,
    imageSource: require('../images/legs.png'),
    exercises: [],
    svg: { fill: '#f172a1' },
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
    count: 0,
  },
  {
    id: 'shoulders',
    key: 5,
    name: 'SHOULDERS',
    activeRoutine: false,
    imageSource: require('../images/shoulder.png'),
    exercises: [],
    svg: { fill: '#e64398' },
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
    count: 0,
  },
];
export const ROUTINESWITHEXERCISES = [
  {
    id: 'abs',
    name: 'ABS',
    // imageSource: require('../images/abs.png'),
    exercises: [
      {
        id: 'Alternate_Heel_Touches_',
        name: 'Alternate Heel Touches ',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Barbell_Ab_Rollout',
        name: 'Barbell Ab Rollout',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Butt_Ups',
        name: 'Butt-Ups',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Cable_Crunch',
        name: 'Cable Crunch',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Cocoons',
        name: 'Cocoons',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Crab_Toe_Touch',
        name: 'Crab Toe Touch',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Cross-Body_Crunch',
        name: 'Cross-Body Crunch',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Crunches',
        name: 'Crunches',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Dead_Bug',
        name: 'Dead Bug',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Decline_Crunches',
        name: 'Decline Crunches',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Decline_Situps',
        name: 'Decline Situps',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Elbow_to_Knee',
        name: 'Elbow to Knee',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Hanging_Leg_Raise',
        name: 'Hanging Leg Raise',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Hanging_Pike',
        name: 'Hanging Pike',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Jackknife_Sit-Up',
        name: 'Jackknife Sit-Up',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Leg_Raises',
        name: 'Leg Raises',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Plank',
        name: 'Plank',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Press_Sit-Up',
        name: 'Press Sit-Up',
        type: 'BARBELL',
      },
      {
        id: 'Reverse_Crunch',
        name: 'Reverse Crunch',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Scissor_Kick',
        name: 'Scissor Kick',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Shoulder_Taps',
        name: 'Shoulder Taps',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Situps',
        name: 'Situps',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Toe_Touchers',
        name: 'Toe Touchers',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Tuck_Crunch',
        name: 'Tuck Crunch',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Weighted_Crunches',
        name: 'Weighted Crunches',
        type: 'DUMBBELL',
      },
    ],
  },
  {
    id: 'back',
    name: 'BACK',
    // imageSource: require('../images/back.png'),
    exercises: [
      {
        id: 'Barbell_Deadlift',
        name: 'Barbell Deadlift',
        type: 'DUMBBELL',
      },
      {
        id: 'Bodyweight_Pullups',
        name: 'Bodyweight Pullups',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Dumbbell_Incline_Row',
        name: 'Dumbbell Incline Row',
        type: 'CABLE MACHINE',
      },
    ],
  },
  {
    id: 'biceps',
    name: 'BICEPS',
    //  imageSource: require('../images/biceps.png'),
    exercises: [
      {
        id: 'Alternate_Hammer_Curl',
        name: 'Alternate Hammer Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Alternate_Incline_Dumbbell_Curl',
        name: 'Alternate Incline Dumbbell Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Barbell_Bicep_Curl_',
        name: 'Barbell Bicep Curl ',
        type: 'BARBELL',
      },
      {
        id: 'Cable_Curl',
        name: 'Cable Curl',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Close-Grip_Standing_Barbell_Curl',
        name: 'Close-Grip Standing Barbell Curl',
        type: 'BARBELL',
      },
      {
        id: 'Concentration_Curls',
        name: 'Concentration Curls',
        type: 'DUMBBELL',
      },
      {
        id: 'Cross_Body_Hammer_Curl',
        name: 'Cross Body Hammer Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Drag_Curl',
        name: 'Drag Curl',
        type: 'BARBELL',
      },
      {
        id: 'Dumbbell_Alternate_Bicep_Curls',
        name: 'Dumbbell Alternate Bicep Curls',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Bicep_Curls',
        name: 'Dumbbell Bicep Curls',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Drag_Curl',
        name: 'Dumbbell Drag Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Prone_Incline_Curl_',
        name: 'Dumbbell Prone Incline Curl ',
        type: 'DUMBBELL',
      },
      {
        id: 'Incline_Dumbbell_Curl',
        name: 'Incline Dumbbell Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Incline_Hammer_Curls',
        name: 'Incline Hammer Curls',
        type: 'DUMBBELL',
      },
      {
        id: 'Machine_Preacher_Curls',
        name: 'Machine Preacher Curls',
        type: 'PREACHER CURL MACHINE',
      },
      {
        id: 'Overhead_Cable_Curl',
        name: 'Overhead Cable Curl',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Preacher_Curls',
        name: 'Preacher Curls',
        type: 'BARBELL',
      },
      {
        id: 'Preacher_Hammer_Dumbbell_C...',
        name: 'Preacher Hammer Dumbbell C...',
        type: 'DUMBBELL',
      },
      {
        id: 'Reverse_Barbell_Curl',
        name: 'Reverse Barbell Curl',
        type: 'BARBELL',
      },
      {
        id: 'Reverse_Cable_Curl',
        name: 'Reverse Cable Curl',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Reverse_Plate_Curls',
        name: 'Reverse Plate Curls',
        type: 'PLATE',
      },
      {
        id: 'Reverse_Preacher_Curls',
        name: 'Reverse Preacher Curls',
        type: 'BARBELL',
      },
      {
        id: 'Seated_Close-Grip_Concentrati...',
        name: 'Seated Close-Grip Concentrati...',
        type: 'BARBELL',
      },
      {
        id: 'Seated_Dumbbell_Curl',
        name: 'Seated Dumbbell Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Seated_Dumbbell_Inner_Biceps...',
        name: 'Seated Dumbbell Inner Biceps...',
        type: 'DUMBBELL',
      },
      {
        id: 'Standing_Concentration_Curl',
        name: 'Standing Concentration Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Standing_Dumbbell_Reverse_Cu...',
        name: 'Standing Dumbbell Reverse Cu...',
        type: 'DUMBBELL',
      },
      {
        id: 'Standing_Inner-Biceps_Curl',
        name: 'Standing Inner-Biceps Curl',
        type: 'DUMBBELL',
      },
      {
        id: 'Two-Arm_Dumbbell_Preacher_C...',
        name: 'Two-Arm Dumbbell Preacher C...',
        type: 'DUMBBELL',
      },
    ],
  },
  {
    id: 'calves',
    name: 'CALVES',
    // imageSource: require('../images/calves.png'),
    exercises: [
      {
        id: 'Leg_Press_Calf_Press',
        name: 'Leg Press Calf Press',
        type: 'LEG PRESS MACHINE',
      },
      {
        id: 'Seated_Barbell_Calf_Raise',
        name: 'Seated Barbell Calf Raise',
        type: 'BARBELL',
      },
      {
        id: 'Seated_Machine_Calf_Raises',
        name: 'Seated Machine Calf Raises',
        type: 'SEATED CLAF RAISE MACHINE',
      },
      {
        id: 'Smith_Machine_Calf_Raises',
        name: 'Smith Machine Calf Raises',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Standing_Dumbbell_Calf_Raises',
        name: 'Standing Dumbbell Calf Raises',
        type: 'DUMBBELL',
      },
    ],
  },
  {
    id: 'cardio',
    name: 'CARDIO',
    // imageSource: require('../images/cardio.png'),
    exercises: [
      {
        id: 'Elliptical_',
        name: 'Elliptical ',
        type: 'ELLIPTICAL MACHINE',
      },
      {
        id: 'Rowing',
        name: 'Rowing',
        type: 'ROWING MACHINE',
      },
      {
        id: 'Spine_Bike',
        name: 'Spine Bike',
        type: 'SPINE BIKE',
      },
      {
        id: 'Stairmaster',
        name: 'Stairmaster',
        type: 'STAIRMASTER',
      },
      {
        id: 'Stationary_Bike',
        name: 'Stationary Bike',
        type: 'STATIONARY BIKE',
      },
      {
        id: 'Treadmill',
        name: 'Treadmill',
        type: 'TREADMILL',
      },
    ],
  },
  {
    id: 'chest',
    name: 'CHEST',
    // imageSource: require('../images/chest.png'),
    exercises: [
      {
        id: 'Barbell_Bench_Press',
        name: 'Barbell Bench Press',
        type: 'BARBELL',
      },
      {
        id: 'Cable_Cross_Overs',
        name: 'Cable Cross Overs',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Cable_Upper_Cross_Overs',
        name: 'Cable Upper Cross Overs',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Crush-Grip_Dumbbell_Bench_Pr...',
        name: 'Crush-Grip Dumbbell Bench Pr...',
        type: 'DUMBBELL',
      },
      {
        id: 'Decline_Dumbbell_Bench_Press',
        name: 'Decline Dumbbell Bench Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Decline_Dumbbell_Flyes',
        name: 'Decline Dumbbell Flyes',
        type: 'DUMBBELL',
      },
      {
        id: 'Decline_Pushups',
        name: 'Decline Pushups',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Dumbbell_Bench_Press',
        name: 'Dumbbell Bench Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Bridge_Chest_Press',
        name: 'Dumbbell Bridge Chest Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Fly',
        name: 'Dumbbell Fly',
        type: 'DUMBBELL',
      },
      {
        id: 'Incline_Barbell_Bench_Press',
        name: 'Incline Barbell Bench Press',
        type: 'BARBELL',
      },
      {
        id: 'Incline_Bench_Cable_Fly',
        name: 'Incline Bench Cable Fly',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Incline_Dumbbell_Bench_Press',
        name: 'Incline Dumbbell Bench Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Incline_Dumbbell_Flyes',
        name: 'Incline Dumbbell Flyes',
        type: 'DUMBBELL',
      },
      {
        id: 'Incline_Pushups',
        name: 'Incline Pushups',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Low_Cable_Crossover',
        name: 'Low Cable Crossover',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Machine_Chest_Fly',
        name: 'Machine Chest Fly',
        type: 'CHEST FLY MACHINE',
      },
      {
        id: 'Machine_Chest_Press',
        name: 'Machine Chest Press',
        type: 'CHEST PRESS MACHINE',
      },
      {
        id: 'Pushups',
        name: 'Pushups',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Smith_Machine_Bench_Press',
        name: 'Smith Machine Bench Press',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Smith_Machine_Decline_Bench...',
        name: 'Smith Machine Decline Bench...',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Smith_Machine_Incline_Bench_Pr...',
        name: 'Smith Machine Incline Bench Pr...',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Straight_Arm_Pullover',
        name: 'Straight Arm Pullover',
        type: 'DUMBBELL',
      },
      {
        id: 'Twisting_Dumbbell_Bench_Press',
        name: 'Twisting Dumbbell Bench Press',
        type: 'DUMBBELL',
      },
    ],
  },
  {
    id: 'forearms',
    name: 'FOREARMS',
    // imageSource: require('../images/forearms.png'),
    exercises: [
      {
        id: 'Bodyweight_Hang',
        name: 'Bodyweight Hang',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Palms_Down_Barbell_Wrist_Curl',
        name: 'Palms Down Barbell Wrist Curl',
        type: 'BARBELL',
      },
      {
        id: 'Palms_Up_Barbell_Wrist_Curl',
        name: 'Palms Up Barbell Wrist Curl',
        type: 'BARBELL',
      },
      {
        id: 'Seated_Barbell_Finger_Curls',
        name: 'Seated Barbell Finger Curls',
        type: 'BARBELL',
      },
      {
        id: 'Standing_Behind-the-back_Wri...',
        name: 'Standing Behind-the-back Wri...',
        type: 'BARBELL',
      },
    ],
  },
  {
    id: 'glutes',
    name: 'GLUTES',
    // imageSource: require('../images/glutes.png'),
    exercises: [
      {
        id: 'Barbell_Bench_Hip_Thrust',
        name: 'Barbell Bench Hip Thrust',
        type: 'BARBELL',
      },
      {
        id: 'Barbell_Glute_Bridge',
        name: 'Barbell Glute Bridge',
        type: 'BARBELL',
      },
      {
        id: 'Glute_Kickback',
        name: 'Glute Kickback',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Lying_Bench_Flutter_Kicks',
        name: 'Lying Bench Flutter Kicks',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Single_Leg_Glute_Bridge',
        name: 'Single Leg Glute Bridge',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Glute_Bridge',
        name: 'Glute Bridge',
        type: 'BODYWEIGHT',
      },
    ],
  },
  {
    id: 'hamstrings',
    name: 'HAMSTRINGS',
    // imageSource: require('../images/hamstrings.png'),
    exercises: [
      {
        id: 'Lying_Leg_Curls',
        name: 'Lying Leg Curls',
        type: 'LEG CURL MACHINE',
      },
      {
        id: 'Seated_Leg_Curls',
        name: 'Seated Leg Curls',
        type: 'LEG CURL MACHINE',
      },
      {
        id: 'Single_Leg_Deadlift_',
        name: 'Single Leg Deadlift ',
        type: 'DUMBBELL',
      },
      {
        id: 'Stiff_Legged_Dumbbell_Deadlift',
        name: 'Stiff Legged Dumbbell Deadlift',
        type: 'DUMBBELL',
      },
    ],
  },
  {
    id: 'lats',
    name: 'LATS',
    // imageSource: require('../images/lats.png'),
    exercises: [
      {
        id: 'Behind_Neck_Lat_Pulldown',
        name: 'Behind Neck Lat Pulldown',
        type: 'LAT PULLDOWN MACHINE',
      },
      {
        id: 'Cable_Incline_Pushdown',
        name: 'Cable Incline Pushdown',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Lat_Pulldown',
        name: 'Lat Pulldown',
        type: 'LAT PULLDOWN MACHINE',
      },
      {
        id: 'Standing_Straight_Arm_Pushdo...',
        name: 'Standing Straight Arm Pushdo...',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Underhand_Cable_Pulldowns',
        name: 'Underhand Cable Pulldowns',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Wide_Grip_Pullups',
        name: 'Wide Grip Pullups',
        type: 'BODYWEIGHT',
      },
    ],
  },
  {
    id: 'lower back',
    name: 'LOWER BACK',
    // imageSource: require('../images/lower back.png'),
    exercises: [
      {
        id: 'Barbell_Goodmornings',
        name: 'Barbell Goodmornings',
        type: 'BARBELL',
      },
      {
        id: 'Barbell_Rack_Pulls',
        name: 'Barbell Rack Pulls',
        type: 'BARBELL',
      },
      {
        id: 'Barbell_Romanian_Deadlift',
        name: 'Barbell Romanian Deadlift',
        type: 'BARBELL',
      },
      {
        id: 'Dumbbell_Deadlift',
        name: 'Dumbbell Deadlift',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Romanian_Deadlift',
        name: 'Dumbbell Romanian Deadlift',
        type: 'DUMBBELL',
      },
      {
        id: 'Hyperextensions',
        name: 'Hyperextensions',
        type: 'HYPEREXTENSION BENCH',
      },
      {
        id: 'Kettlebell_Romanian_Deadlift',
        name: 'Kettlebell Romanian Deadlift',
        type: 'KETTLEBELL',
      },
      {
        id: 'Seated_Barbell_Goodmornings',
        name: 'Seated Barbell Goodmornings',
        type: 'BARBELL',
      },
      {
        id: 'Superman',
        name: 'Superman',
        type: 'BODYWEIGHT',
      },
    ],
  },
  {
    id: 'middle back',
    name: 'MIDDLE BACK',
    // imageSource: require('../images/middle back.png'),
    exercises: [
      {
        id: 'Alternating_Kettlebell_Row',
        name: 'Alternating Kettlebell Row',
        type: 'KETTLEBELL',
      },
      {
        id: 'Barbell_Bent_Over_Bar_Row',
        name: 'Barbell Bent Over Bar Row',
        type: 'BARBELL',
      },
      {
        id: 'Bent_Over_Dumbbell_Rows',
        name: 'Bent Over Dumbbell Rows',
        type: 'DUMBBELL',
      },
      {
        id: 'Bent_Over_Rows',
        name: 'Bent Over Rows',
        type: 'BARBELL',
      },
      {
        id: 'Incline_Bench_Pull',
        name: 'Incline Bench Pull',
        type: 'BARBELL',
      },
      {
        id: 'Kettlebell_Row',
        name: 'Kettlebell Row',
        type: 'KETTLEBELL',
      },
      {
        id: 'Middle_Back_Shrug',
        name: 'Middle Back Shrug',
        type: 'DUMBBELL',
      },
      {
        id: 'One_Arm_Dumbbell_Rows',
        name: 'One Arm Dumbbell Rows',
        type: 'DUMBBELL',
      },
      {
        id: 'One_Arm_Seated_Cable_Rows',
        name: 'One Arm Seated Cable Rows',
        type: 'CABLE ROW MACHINE',
      },
      {
        id: 'Reverse_Grip_Bent_Over_Rows',
        name: 'Reverse Grip Bent Over Rows',
        type: 'BARBELL',
      },
      {
        id: 'Seated_Cable_Rows',
        name: 'Seated Cable Rows',
        type: 'CABLE ROW MACHINE',
      },
      {
        id: 'Smith_Machine_Bent_Over_Row',
        name: 'Smith Machine Bent Over Row',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Smith_Machine_Inverted_Row',
        name: 'Smith Machine Inverted Row',
        type: 'BODYWEIGHT',
      },
    ],
  },
  {
    id: 'obliques',
    name: 'OBLIQUES',
    // imageSource: require('../images/obliques.png'),
    exercises: [
      {
        id: 'Barbell_Side_Bend',
        name: 'Barbell Side Bend',
        type: 'BARBELL',
      },
      {
        id: 'Decline_Oblique_Crunches',
        name: 'Decline Oblique Crunches',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Dumbbell_Side_Bend',
        name: 'Dumbbell Side Bend',
        type: 'DUMBBELL',
      },
      {
        id: 'Oblique_Cable_Crunch',
        name: 'Oblique Cable Crunch',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Oblique_Crunches',
        name: 'Oblique Crunches',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Plate_Twist',
        name: 'Plate Twist',
        type: 'PLATE',
      },
      {
        id: 'Russian_Twist',
        name: 'Russian Twist',
        type: 'EXERCISE BALL',
      },
      {
        id: 'Seated_Barbell_Twist',
        name: 'Seated Barbell Twist',
        type: 'BARBELL',
      },
      {
        id: 'Side_Bridge',
        name: 'Side Bridge',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Side_Jackknifes',
        name: 'Side Jackknifes',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Standing_Cable_Lift',
        name: 'Standing Cable Lift',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Windscreen_Wiper',
        name: 'Windscreen Wiper',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Woodchop',
        name: 'Woodchop',
        type: 'CABLE MACHINE',
      },
    ],
  },
  {
    id: 'quadriceps',
    name: 'QUADRICEPS',
    // imageSource: require('../images/quadriceps.png'),
    exercises: [
      {
        id: 'Barbell_Hack_Squats',
        name: 'Barbell Hack Squats',
        type: 'BARBELL',
      },
      {
        id: 'Barbell_Squats',
        name: 'Barbell Squats',
        type: 'BARBELL',
      },
      {
        id: 'Bodyweight_Squats',
        name: 'Bodyweight Squats',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Dumbbell_Lunges',
        name: 'Dumbbell Lunges',
        type: 'DUMBBELL',
      },
      {
        id: 'Dumbbell_Squats',
        name: 'Dumbbell Squats',
        type: 'DUMBBELL',
      },
      {
        id: 'Front_Barbell_Squats',
        name: 'Front Barbell Squats',
        type: 'BARBELL',
      },
      {
        id: 'Goblet_Squats',
        name: 'Goblet Squats',
        type: 'KETTLEBELL',
      },
      {
        id: 'Leg_Extensions',
        name: 'Leg Extensions',
        type: 'LEG EXTENSION MACHINE',
      },
      {
        id: 'Lunges',
        name: 'Lunges',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Machine_Hack_Squats',
        name: 'Machine Hack Squats',
        type: 'HACK SQUAT MACHINE',
      },
      {
        id: 'Machine_Leg_Press',
        name: 'Machine Leg Press',
        type: 'LEG PRESS MACHINE',
      },
      {
        id: 'Smith_Machine_Squats',
        name: 'Smith Machine Squats',
        type: 'SMITH MACHINE',
      },
    ],
  },
  {
    id: 'shoulders',
    name: 'SHOULDERS',
    // imageSource: require('../images/shoulders.png'),
    exercises: [
      {
        id: 'Standing_Barbell_Shoulder_Press',
        name: 'Standing Barbell Shoulder Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Arnold_Dumbbell_Press',
        name: 'Arnold Dumbbell Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Barbell_Upright_Row',
        name: 'Barbell Upright Row',
        type: 'BARBELL',
      },
      {
        id: 'Cable_Rear_Delt_Flyes',
        name: 'Cable Rear Delt Flyes',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Front_Dumbbell_Raises',
        name: 'Front Dumbbell Raises',
        type: 'DUMBBELL',
      },
      {
        id: 'Lateral_Dumbbell_Raises',
        name: 'Lateral Dumbbell Raises',
        type: 'DUMBBELL',
      },
      {
        id: 'Machine_Shoulder_Press',
        name: 'Machine Shoulder Press',
        type: 'SHOULDER PRESS MACHINE',
      },
      {
        id: 'Palms_In_Shoulder_Press',
        name: 'Palms In Shoulder Press',
        type: 'DUMBBELL',
      },
      {
        id: 'Rear_Delt_Dumbbell_Raises',
        name: 'Rear Delt Dumbbell Raises',
        type: 'DUMBBELL',
      },
      {
        id: 'Reverse_Machine_Flyes',
        name: 'Reverse Machine Flyes',
        type: 'REVERSE FLY MACHINE',
      },
      {
        id: 'Seated_Barbell_Shoulder_Press',
        name: 'Seated Barbell Shoulder Press',
        type: 'BARBELL',
      },
      {
        id: 'Seated_Dumbbell_Shoulder_Pre...',
        name: 'Seated Dumbbell Shoulder Pre...',
        type: 'DUMBBELL',
      },
      {
        id: 'Seated_Lateral_Dumbbell_Raises',
        name: 'Seated Lateral Dumbbell Raises',
        type: 'DUMBBELL',
      },
      {
        id: 'Seated_Rear_Delt_Dumbbell_Ra...',
        name: 'Seated Rear Delt Dumbbell Ra...',
        type: 'DUMBBELL',
      },
      {
        id: 'Smith_Machine_Upright_Row',
        name: 'Smith Machine Upright Row',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Standing_Dumbbell_Shoulder_P...',
        name: 'Standing Dumbbell Shoulder P...',
        type: 'DUMBBELL',
      },
    ],
  },
  {
    id: 'traps',
    name: 'TRAPS',
    // imageSource: require('../images/traps.png'),
    exercises: [
      {
        id: 'Barbell_Shrugs',
        name: 'Barbell Shrugs',
        type: 'BARBELL',
      },
      {
        id: 'Cable_Shrugs',
        name: 'Cable Shrugs',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Dumbbell_Shrugs',
        name: 'Dumbbell Shrugs',
        type: 'DUMBBELL',
      },
      {
        id: 'Rear_Smith_Machine_Shrugs',
        name: 'Rear Smith Machine Shrugs',
        type: 'SMITH MACHINE',
      },
      {
        id: 'Smith_Machine_Shrugs_',
        name: 'Smith Machine Shrugs ',
        type: 'SMITH MACHINE',
      },
    ],
  },
  {
    id: 'triceps',
    name: 'TRICEPS',
    // imageSource: require('../images/triceps.png'),
    exercises: [
      {
        id: 'Bench_Dips',
        name: 'Bench Dips',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Bent_Over_Tricep_Extensions',
        name: 'Bent Over Tricep Extensions',
        type: 'DUMBBELL',
      },
      {
        id: 'Close_Grip_Bench_Press',
        name: 'Close Grip Bench Press',
        type: 'BARBELL',
      },
      {
        id: 'Decline_Tricep_Extensions',
        name: 'Decline Tricep Extensions',
        type: 'BARBELL',
      },
      {
        id: 'Diamond_Pushups',
        name: 'Diamond Pushups',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Kneeling_Tricep_Cable_Extension',
        name: 'Kneeling Tricep Cable Extension',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Reverse_Tricep_Pushdown',
        name: 'Reverse Tricep Pushdown',
        type: 'CABLE MACHINE',
      },
      {
        id: 'Skull_Crusher',
        name: 'Skull Crusher',
        type: 'BARBELL',
      },
      {
        id: 'Tricep_Dips',
        name: 'Tricep Dips',
        type: 'BODYWEIGHT',
      },
      {
        id: 'Tricep_Dumbbell_Kickback',
        name: 'Tricep Dumbbell Kickback',
        type: 'DUMBBELL',
      },
      {
        id: 'Tricep_Pushdown',
        name: 'Tricep Pushdown',
        type: 'CABLE MACHINE',
      },
    ],
  },
];
export const ROUTINESIDS = ['abs', 'back', 'chest', 'legs', 'shoulders'];
// const ABS = {
//   '12/04/2019': {
//     totalWeights: 200
//   },
//   '1561108849779': {
//     totalWeights: 150
//   },
//   '1561108849779': {
//     totalWeights: 130
//   },
//   '1561108849779': {
//     totalWeights: 120
//   },
//   '1561108849779': {
//     totalWeights: 100
//   },
//   '1561108849779': {
//     totalWeights: 40
//   },
//   '1561108849779': {
//     totalWeights: 30
//   },
//   abs_weight_arr=['200', '150', '130', '120']
// }
