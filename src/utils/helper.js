import AsyncStorage from '@react-native-community/async-storage';

export const saveTotals = async (key, date, weight) => {
  try {
    const previousRoutineDataJson = await AsyncStorage.getItem(key);
    if (previousRoutineDataJson) {
      const deSerialized = JSON.parse(previousRoutineDataJson);
      const datesArr = Object.keys(deSerialized);
      if (datesArr.indexOf(date) > -1) {
        deSerialized[date].totalWeightForToday = deSerialized[date].totalWeightForToday + weight;
        const serialized = JSON.stringify(deSerialized);
        await AsyncStorage.setItem(key, serialized);
      } else {
        const newRoutineData = {
          ...deSerialized,
          [date]: { totalWeightForToday: weight },
        };
        await AsyncStorage.setItem(key, JSON.stringify(newRoutineData));
      }
    } else {
      const newRoutineData = {
        [date]: { totalWeightForToday: weight },
      };
      const serialized = JSON.stringify(newRoutineData);
      await AsyncStorage.setItem(key, serialized);
    }
  } catch (err) {
    // console.log('errerrerr', err);
    return Promise.reject(err);
  }
};

export const getDateString = () => {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
  const yyyy = today.getFullYear();

  return `${mm}/${dd}/${yyyy}`;
};
