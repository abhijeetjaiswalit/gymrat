import React from 'react';
import { Platform, StatusBar } from 'react-native';
import { Icon } from 'native-base';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator,
  createAppContainer,
} from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
// import HomeScreen1 from './screens/HomeScreen1';
import RoutineScreen from './screens/RoutineScreen';
import StatisticScreen from './screens/StatisticScreen';
import SuggestionsScreen from './screens/SuggestionsScreen';

// const headerStyle = {
//   marginTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
// };
// const { width } = Dimensions.get('window');
const getTabBarIcon = iconName => {
  return <Icon name={iconName} size={30} />;
};

const HomeStack = createBottomTabNavigator(
  {
    Home2: {
      screen: RoutineScreen,
      navigationOptions: {
        tabBarLabel: 'Routine',
        tabBarIcon: ({ tintColor }) => getTabBarIcon('ios-repeat', tintColor),
      },
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor }) => getTabBarIcon('home', tintColor),
      },
    },
    Home3: {
      screen: StatisticScreen,
      navigationOptions: {
        tabBarLabel: 'Stats',
        tabBarIcon: ({ tintColor }) => getTabBarIcon('ios-stats', tintColor),
      },
    },
    Home4: {
      screen: SuggestionsScreen,
      navigationOptions: {
        tabBarLabel: 'Suggestions',
        tabBarIcon: ({ tintColor }) => getTabBarIcon('ios-help-circle', tintColor),
      },
    },
  },
  {
    lazy: true,
  }
);
export const AppStack = createStackNavigator(
  {
    Home2: {
      screen: HomeStack,
    },
  },
  {
    headerMode: 'none',
    tabBarOptions: {
      style: {
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
      },
    },
  }
);

export const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      App: AppStack,
    },
    {
      headerMode: 'none',
      initialRouteName: 'App',
    }
  )
);
